package validation

import (
	"gopkg.in/go-playground/validator.v9"
	"log"
	"main/lib"
	"strconv"
	"strings"
)

/**
*共通系バリデーション
*/
// 複数id
func CheckIds(fl validator.FieldLevel) bool {
	id := fl.Field().String()
	//数値判定
	ids := strings.Split(id, ",")
	for _, v := range ids {
		if len(v) == 0 {
			continue
		}
		_, err := strconv.Atoi(v)
		if err != nil {
			log.Printf("## not num##")
			return false
		}
	}
	return true
}

// 単一id
func CheckId(fl validator.FieldLevel) bool {
	id := fl.Field().String()
	// 空の場合は許容
	if len(id) == 0 {
		return true
	}
	//単一数値判定
	_, err := strconv.Atoi(id)
	if err != nil {
		log.Printf("## not num##")
		return false
	}
	return true
}

func CheckFlg(fl validator.FieldLevel) bool {
	flg := fl.Field().String()
	// 空の場合は許容
	if len(flg) == 0 {
		return true
	}
	//0 or 1判定
	if flg == "0" || flg == "1" {
		return true
	}
	log.Printf("## not 0 or 1##v", flg)
	return false
}
