package model

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

// 正常系テスト
func TestArticlesNormalTest(t *testing.T) {
	// Setup
	am := NewArticlesModel()

	// １ジャンル取得(存在しないid:0)
	ids := []int{0}
	isDelete := -1
	res1 := am.GetArticles(ids, isDelete)
	assert.Empty(t, res1)

	// １ジャンル取得(id:1)
	ids = []int{1}
	res2 := am.GetArticles(ids, isDelete)
	assert.NotEmpty(t, res2)
	assert.Condition(t, func() bool { if len(res2) >= len(res1) { return true }; return false })

	// 複数ジャンル取得(id:1, id:2)
	ids = []int{1, 2}
	res3 := am.GetArticles(ids, isDelete)
	assert.NotEmpty(t, res3)
	assert.NotEqual(t, res2, res3)
	assert.Condition(t, func() bool { if len(res3) >= len(res2) { return true }; return false })

	// 全ジャンル取得
	ids = []int{}
	res4 := am.GetArticles(ids, isDelete)
	assert.NotEmpty(t, res4)
	assert.NotEqual(t, res2, res4)
	assert.Condition(t, func() bool { if len(res4) >= len(res2) { return true }; return false })

	// 削除フラグ 0
	isDelete = 0
	res5 := am.GetArticles(ids, isDelete)
	assert.Condition(t, func() bool { if len(res4) >= len(res5) { return true }; return false })

	// 削除フラグ 1
	isDelete = 1
	res6 := am.GetArticles(ids, isDelete)
	assert.Condition(t, func() bool { if len(res5) >= len(res6) { return true }; return false })
}

// 異常系テスト (静的型付言語のため、文字列をメソッド引数に渡すとかコンパイルに通らないことはそもそもできないので現状テスト項目なし)
//func TestGenresAbnormalTest(t *testing.T) {
//}