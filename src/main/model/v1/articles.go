package model

import ()

type (
	ArticlesModel interface {
		GetArticles(ids []int, isDelete int) []Genre
	}

	articlesModel struct {
		GenresModel
	}

	Article struct {
		Id         int    `json:"id"`
		Name       string `json:"name"`
		Is_Delete  int    `json:"is_delete"`
		Created_At string `json:"created_at"`
		Updated_At string `json:"updated_at"`
	}
)

func NewArticlesModel() *articlesModel {
	return &articlesModel{}
}

func (u *articlesModel) GetArticles(ids []int, isDelete int) []Article {
	g := []Article{}
	db := getGormConnect()
	if len(ids) != 0 {
		db = db.Where("id in (?)", ids)
	}
	if isDelete != -1 {
		db = db.Where("is_delete = ?", isDelete)
	}
	db.Find(&g)
	defer db.Close()

	return g
}
