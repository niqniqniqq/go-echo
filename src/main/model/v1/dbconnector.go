package model

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"main/config"
	"os"
)

func getGormConnect() *gorm.DB {
	user := "db_user"
	pass := "db_pass"
	protocol := "protocol"
	dbName := "dbName"
	dbms := "dbms"

	con := user + ":" + pass + "@" + protocol + "/" + dbName
	db, err := gorm.Open(dbms, con)

	if err != nil {
		panic(err.Error())
	}

	return db
}
