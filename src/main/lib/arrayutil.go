package lib

import (
	"strconv"
	"time"
	"math/rand"
)

func InArray(val string, array []string) bool {
	isExists := false
	for _, v := range array {
		if val == v {
			isExists = true
			return isExists
		}
	}
	return isExists
}

func ToIntArray(array []string) []int {
	nums := []int{}
	for _, v := range array {
		n, err := strconv.Atoi(v)
		if err == nil {
			nums = append(nums, n)
		}
	}
	return nums
}

func Shuffle(vals []int) {
  r := rand.New(rand.NewSource(time.Now().UnixNano()))
  for n := len(vals); n > 0; n-- {
    randIndex := r.Intn(n)
    vals[n-1], vals[randIndex] = vals[randIndex], vals[n-1]
  }
}
