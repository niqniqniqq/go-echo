package route

import (
	"github.com/labstack/echo"
	"main/api/v1"
	"main/model/v1"
)

func Init(e *echo.Echo) *echo.Echo {
	// v1 Routes
	e1 := e.Group("/api/v1")
	{
		// Articles
		ma := model.NewArticlesModel()
		a := api.NewArticles(ma)
		e1.GET("/articles", a.GetArticles)
	}

	return e
}
