package api

import (
	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"main/model/v1"
	"net/http"
	"net/http/httptest"
	"testing"
)

type articlesModelStub struct {
}

// tagModelインタフェースを満たす実装
func (g *articlesModelStub) GetArticles(id []int, isDelete int) []model.Article { return []model.Article{} }

// 正常系テスト
func TestArticlesNormalTest(t *testing.T) {
	// Setup
	e := echo.New()
	as := &articlesModelStub{}
	h := &articles{V1{}, as}
	rec := httptest.NewRecorder()

	// パラムなしテスト
	req := httptest.NewRequest(echo.GET, "/api/vi/articles", nil)
	c := e.NewContext(req, rec)
	h.GetArticles(c)
	assert.Equal(t, http.StatusOK, rec.Code)

	// 単体パラム指定テスト
	req = httptest.NewRequest(echo.GET, "/api/vi/articles?id=1", nil)
	c = e.NewContext(req, rec)
	h.GetArticles(c)
	assert.Equal(t, http.StatusOK, rec.Code)

	// 複数パラムテスト
	req = httptest.NewRequest(echo.GET, "/api/vi/articles?id=1,2,3", nil)
	c = e.NewContext(req, rec)
	h.GetArticles(c)
	assert.Equal(t, http.StatusOK, rec.Code)

	// 削除フラグ0
	req = httptest.NewRequest(echo.GET, "/api/vi/articles?is_delete=0", nil)
	c = e.NewContext(req, rec)
	h.GetArticles(c)
	assert.Equal(t, http.StatusOK, rec.Code)

	// 削除フラグ1
	req = httptest.NewRequest(echo.GET, "/api/vi/articles?is_delete=1", nil)
	c = e.NewContext(req, rec)
	h.GetArticles(c)
	assert.Equal(t, http.StatusOK, rec.Code)

	// 仕様に存在しないパラメータは無視
	req = httptest.NewRequest(echo.GET, "/api/vi/articles?test=1&is_delete=1", nil)
	c = e.NewContext(req, rec)
	h.GetArticles(c)
	assert.Equal(t, http.StatusOK, rec.Code)
}

// 異常系テスト
func TestArticlesAbnormalTest(t *testing.T) {
	// Setup
	e := echo.New()
	as := &articlesModelStub{}
	h := &articles{V1{}, as}
	rec := httptest.NewRecorder()

	// 不正ID指定テスト
	req := httptest.NewRequest(echo.GET, "/api/vi/articles?id=a", nil)
	c := e.NewContext(req, rec)
	h.GetArticles(c)
	assert.Equal(t, http.StatusBadRequest, rec.Code)

	// 不正削除フラグ指定テスト
	req = httptest.NewRequest(echo.GET, "/api/vi/articles?is_delete=a", nil)
	c = e.NewContext(req, rec)
	h.GetArticles(c)
	assert.Equal(t, http.StatusBadRequest, rec.Code)

	// 正常ID不正ID混合指定テスト
	req = httptest.NewRequest(echo.GET, "/api/vi/articles?id=1,a", nil)
	c = e.NewContext(req, rec)
	h.GetArticles(c)
	assert.Equal(t, http.StatusBadRequest, rec.Code)
}
