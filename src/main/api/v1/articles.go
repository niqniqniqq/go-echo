package api

import (
	"github.com/labstack/echo"
	"gopkg.in/go-playground/validator.v9"
	"log"
	"main/lib"
	"main/model/v1"
	"main/validation/v1"
	"net/http"
	"strings"
)

type (
	articles struct {
		v1          V1
		articlesModel model.ArticlesModel
	}
	ArticleUrlParams struct {
		Id       string `validate:"checkIds"`
		IsDelete string `validate:"checkFlg"`
	}
)

// Validate is UrlParams validater
func (p *ArticleUrlParams) Validate() error {
	validate := validator.New()
	validate.RegisterValidation("checkIds", validation.CheckIds)
	validate.RegisterValidation("checkFlg", validation.CheckFlg)
	return validate.Struct(p)
}

func NewArticles(t model.ArticlesModel) *articles {
	return &articles{V1{}, t}
}

func (t articles) GetArticles(c echo.Context) error {
	// param取得
	id := c.QueryParam("id")
	isDelete := c.QueryParam("is_delete")

	// paramバリデーション
	up := ArticleUrlParams{Id: id, IsDelete: isDelete}
	err := up.Validate()
	if err != nil {
		log.Printf("##error##%v", err)
		return c.JSON(http.StatusBadRequest, t.v1.GetErrorResponse(http.StatusBadRequest, err, http.StatusBadRequest))
	}

	//DBから取得
	ids := lib.ToIntArray(strings.Split(id, ","))
	numDelete := t.v1.ConvertParamToInt(isDelete)
	ret := t.articlesModel.GetArticles(ids, numDelete)

	return c.JSON(http.StatusOK, t.v1.GetSuccessResponse(http.StatusOK, ret))
}
