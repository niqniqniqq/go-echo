package api

import (
	"strconv"
)

type (
	V1 struct {
		Api
	}

	Api interface {
		GetSuccessResponse(sc int, res interface{}) SuccessResponse
		GetErrorResponse(sc int, mess error, ec int) ErrorResponse
	}

	SuccessMetaData struct {
		Status_code int    `json:"status_code"`
		Http_method string `json:"http_method"`
	}

	ErrorMetaData struct {
		Status_code int    `json:"status_code"`
		Message     string `json:"message"`
		Error_code  int    `json:"error_code"`
		Http_method string `json:"http_method"`
	}

	SuccessResponse struct {
		MetaData SuccessMetaData `json:"metadata"`
		Result   interface{}     `json:"result"`
	}

	ErrorResponse struct {
		MetaData ErrorMetaData `json:"metadata"`
		Result   string        `json:"result"`
	}
)

func (v *V1) GetSuccessResponse(sc int, res interface{}) SuccessResponse {
	smd := SuccessMetaData{Status_code: sc, Http_method: "GET"}
	sr := SuccessResponse{MetaData: smd, Result: res}
	return sr
}

func (v *V1) GetErrorResponse(sc int, err error, ec int) ErrorResponse {
	emd := ErrorMetaData{Status_code: sc, Message: err.Error(), Error_code: ec, Http_method: "GET"}
	er := ErrorResponse{MetaData: emd, Result: "null"}
	return er
}

// stringパラメータをintへ変換します(変換不可の場合-1を返却) p:変換パラム
func (v *V1) ConvertParamToInt(p string) int {
	if len(p) == 0 {
		return -1
	}
	np, err := strconv.Atoi(p)
	if err != nil {
		np = -1
	}
	return np
}

// stringパラメータをintへ変換します(変換不可の場合-1を返却) p:変換パラム d:ディフォルト値
func (v *V1) ConvertParamToIntWithDefalut(p string, d int) int {
	if len(p) == 0 {
		return d
	}
	np, err := strconv.Atoi(p)
	if err != nil {
		np = -1
	}
	return np
}
